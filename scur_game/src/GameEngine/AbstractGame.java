package GameEngine;

/**
 * represente un jeu un jeu est caracterise par la methode update a redefinir
 * 
 * @author Graou
 *
 */
public interface AbstractGame {

	String update();

	boolean isOver();
}
