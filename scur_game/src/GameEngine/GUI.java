package GameEngine;

import Display.AbstractDrawing;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jsfml.graphics.RenderWindow;
import org.jsfml.graphics.View;
import org.jsfml.system.Vector2f;
import org.jsfml.window.VideoMode;


/**
 * cree une interface graphique avec son controleur et son afficheur
 * @author Graou
 *
 */
public class GUI extends RenderWindow {


	private static GUI instance;

	/**
	 * le controleur lie a la JFrame
	 */
	private AbstractDrawing dessin;
	private View viewprincipale;

	private float view_height = 512.0f;


	
	/**
	 * la construction de l'interface grpahique
	 * - construit la JFrame
	 * - construit les Attributs
	 * 
	 * @param afficheurUtil l'afficheur a utiliser dans le moteur
	 * 
	 */
	public GUI(AbstractDrawing afficheurUtil, int x, int y)
	{
	    super();

		instance = this;

		this.create(new VideoMode(x, y), "KiyuAdventure");
		this.dessin = afficheurUtil;

		this.viewprincipale = new View(new Vector2f(x/2, y/2), new Vector2f(view_height, view_height));
		this.resizeView();
		this.setView(viewprincipale);
		this.viewprincipale.zoom(0.5f);
	}


	/**
	 * demande la mise a jour du dessin
	 */
	public void dessiner() {
		this.dessin.draw(this);
	}

	public void centerOnBody(Body b){
		Vec2 pos = b.getPosition();
		float x = pos.x;
		/*
		float x_min = GUI.getInstance().getViewprincipale().getSize().x/2;
		if(x < x_min)
			x = x_min;
		*/
        float y = pos.y;
		y = -y;
		y+= GUI.getInstance().getSize().y;
		this.viewprincipale.setCenter(x, y);
		this.setView(this.viewprincipale);
	}

	public void resizeView(){
		float aspect_ratio = (float)this.getSize().x / (float)this.getSize().y;
		this.viewprincipale.setSize(view_height*aspect_ratio, view_height);
        this.viewprincipale.zoom(0.5f);
        this.setView(this.viewprincipale);
	}

	public View getViewprincipale(){
		return viewprincipale;
	}


    public static GUI getInstance(){
		return instance;
	}
}
