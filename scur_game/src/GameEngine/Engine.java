package GameEngine;

import Display.AbstractDrawing;
import Model.Game.ScurGame;
import org.jsfml.graphics.View;
import org.jsfml.window.Keyboard;
import org.jsfml.window.event.Event;

/**
 * classe Engine represente un moteur de jeu generique.
 *
 * On lui passe un jeu et un afficheur et il permet d'executer un jeu.
 */
public class Engine {



    private GUI gui;
    private AbstractDrawing dessin;
    private boolean centerOnPlayer = true;


    public Engine(AbstractDrawing pAffiche) {
        this.dessin = pAffiche;
    }


    public void lancerJeu(int width, int height, int fps) {

        // creer interface graphique
        this.creerInterface(width, height);

        // duree d'une boucle
        long dureeBoucle = 1000 / fps;
        System.out.println("duree d'une boucle " + dureeBoucle + "ms");
        int nbIteration = 0;

        // on recupere en nanos
        long l = System.currentTimeMillis();
        long beforeTime = System.nanoTime();

        // boucle de jeu
        while (!ScurGame.getInstance().isOver()) {
            // fait update le jeu
            this.manageEvents();
            ScurGame.getInstance().update();

            // affiche le jeu
            if(this.centerOnPlayer) {
                GUI.getInstance().centerOnBody(ScurGame.getInstance().getPlayer().getBody());
            }
            this.gui.dessiner();


            // apres le render en nanos
            long timafter = System.nanoTime();
            // duree en nanos
            long duree = dureeBoucle * 1000000L - (timafter - beforeTime);
            // System.out.println("doit attendre" + duree / 1000L);
            duree = duree / 1000000L;

            // attente dans vide
            if (duree < 0){
                //System.out.println("trop de temps");
            }
            else {
                while (System.nanoTime() - beforeTime - dureeBoucle * 1000000L < 0) {
                }
            }

            beforeTime = System.nanoTime();
            nbIteration++;

        }

        long l2 = System.currentTimeMillis();

        // statistiques
        System.out.println("\n\n\n************************\n");
        System.out.println("FPS = " + (nbIteration * 1000.0 / (l2 - l)));
        System.out.println("\n************************");
        this.gui.close();
    }

    public void manageEvents(){

        Event e;
        while((e = GUI.getInstance().pollEvent()) != null){
            switch (e.type){
                case CLOSED:
                    ScurGame.getInstance().stop();
                    break;
                case RESIZED:
                    GUI.getInstance().resizeView();
                    break;
            }
        }

        //moving the view for debugging
        {
            View v = GUI.getInstance().getViewprincipale();
            if (Keyboard.isKeyPressed(Keyboard.Key.R)) {
                ScurGame.restart();
            }
            if (Keyboard.isKeyPressed(Keyboard.Key.UP)) {
                v.move(0, 5);
            }
            if (Keyboard.isKeyPressed(Keyboard.Key.DOWN)) {
                v.move(0, -5);
            }
            if (Keyboard.isKeyPressed(Keyboard.Key.LEFT)) {
                v.move(-5, 0);
            }
            if (Keyboard.isKeyPressed(Keyboard.Key.RIGHT)) {
                v.move(5, 0);
            }
            if(Keyboard.isKeyPressed(Keyboard.Key.ADD) || Keyboard.isKeyPressed(Keyboard.Key.P)){
                v.zoom(0.9f);
            }
            if(Keyboard.isKeyPressed(Keyboard.Key.SUBTRACT) || Keyboard.isKeyPressed(Keyboard.Key.M)){
                v.zoom(1.1f);
            }
            if(Keyboard.isKeyPressed(Keyboard.Key.C)){
                this.centerOnPlayer = !this.centerOnPlayer;
            }
            GUI.getInstance().setView(v);
        }
    }

    /**
     * methode de creation interface graphique
     *
     * @param width
     *            taille interface
     * @param height
     *            taille interface
     * @return controleur clavier
     */
    private void creerInterface(int width, int height) {
        // creation de l'interface graphique
        this.gui = new GUI(this.dessin, width, height);
        View v = GUI.getInstance().getViewprincipale();
        v.zoom(0.05f);
        this.gui.setView(v);
    }

}
