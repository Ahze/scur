package Main;

import Model.Game.ScurGame;
import Display.AbstractDrawing;
import GameEngine.Engine;
import Display.ScurDraw;

public class Main {

    public static void main(String[] args) throws ClassNotFoundException {
        ScurGame.createInstance();
        AbstractDrawing dessin = new ScurDraw();
        Engine mg = new Engine(dessin);

        mg.lancerJeu(800, 600, 60);
    }

}
