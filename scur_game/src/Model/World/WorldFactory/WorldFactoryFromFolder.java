package Model.World.WorldFactory;

import Display.SpriteFacory.SpriteFactoryForWorld;
import Display.SpriteFacory.SpriteFactory;
import Model.World.ScurWorld;
import org.jsfml.graphics.Sprite;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class WorldFactoryFromFolder implements WorldFactory {

    private String worldFolder;

    public WorldFactoryFromFolder(String spritePath) {
        this.worldFolder = spritePath;
    }


    @Override
    public ScurWorld createWorld() {
        ScurWorld world = null;
        Sprite sprite = new SpriteFactoryForWorld().loadSprite(this.worldFolder);

        try {

            BufferedReader br = new BufferedReader(new FileReader(new File("datas/worlds/" + this.worldFolder + "/details")));
            String json = "", line = "";
            while ((line = br.readLine()) != null) {
                json += line + " ";
            }
            JSONObject o = new JSONObject(json);
            //TODO finir creation du monde par json

            float x_max = sprite.getTexture().getSize().x / SpriteFactory.scale;
            float y_max = sprite.getTexture().getSize().y / SpriteFactory.scale;
            world = new ScurWorld(x_max, y_max, sprite, o.getString("name"));

            //creates world's walls
            JSONArray walls = o.getJSONArray("walls");
            for(int i = 0; i < walls.length(); i++){
                JSONObject wall = (JSONObject)walls.get(i);
                float x = (float)wall.getDouble("x");
                float y = (float)wall.getDouble("y");
                float width = (float)wall.getDouble("width");
                float height = (float)wall.getDouble("height");
                world.createRectangularEntity(x, y, width, height, false).setInvisible(true);//.setInvisible(true);
                System.out.println(wall);
            }

            //creates entities
            JSONArray entities = o.getJSONArray("entities");
            for(int i = 0; i < entities.length(); i++){
                JSONObject entity = (JSONObject)entities.get(i);
                float x = (float)entity.getDouble("x");
                float y = (float)entity.getDouble("y");
                String spritePath = entity.getString("sprite");
                boolean dynamic = entity.getBoolean("dynamic");
                world.createEntityFromSprite(x, y, dynamic, spritePath);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        return world;
    }
}
