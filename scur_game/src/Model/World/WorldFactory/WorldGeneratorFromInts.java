package Model.World.WorldFactory;

import Model.World.ScurWorld;

public class WorldGeneratorFromInts implements WorldFactory{

    private float x_max, y_max;
    private String world_name;

    public WorldGeneratorFromInts(float x_max, float y_max, String name) {
        this.x_max = x_max;
        this.y_max = y_max;
        this.world_name = name;
    }

    public ScurWorld createWorld(){
        return new ScurWorld(x_max, y_max, world_name);
    }

}
