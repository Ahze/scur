package Model.World.WorldFactory;

import Model.World.ScurWorld;

import java.io.IOException;

public interface WorldFactory {

    ScurWorld createWorld();

}
