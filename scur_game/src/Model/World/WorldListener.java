package Model.World;

import Model.Entities.Entity;
import org.jbox2d.callbacks.ContactImpulse;
import org.jbox2d.callbacks.ContactListener;
import org.jbox2d.collision.Manifold;
import org.jbox2d.dynamics.Fixture;
import org.jbox2d.dynamics.contacts.Contact;

public class WorldListener implements ContactListener {

    private Fixture playerFixure;

    public WorldListener(Fixture playerFixure) {
        this.playerFixure = playerFixure;
    }

    int i = 0;

    @Override
    public void beginContact(Contact contact) {
        if(contact.getFixtureA() == playerFixure)
            System.out.println(Entity.entityMap.get(contact.getFixtureB().getBody()));
        if(contact.getFixtureB() == playerFixure) {
            System.out.println(Entity.entityMap.get(contact.getFixtureA().getBody()));
        }
        System.out.println(i++);
    }

    @Override
    public void endContact(Contact contact) {

    }

    @Override
    public void preSolve(Contact contact, Manifold manifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse contactImpulse) {

    }
}
