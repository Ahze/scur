package Model.World;

import Display.SpriteFacory.SpriteFactoryForEntity;
import Display.SpriteFacory.SpriteFactory;
import GameEngine.GUI;
import Model.Entities.Entity;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.*;
import org.jsfml.graphics.*;
import org.jsfml.system.Vector2f;

public class ScurWorld extends World implements Drawable {

    private float x_max, y_max;
    private Sprite sprite;
    private String name;

    public ScurWorld(float x_max, float y_max, String name) {
        super(new Vec2(0f, 0f), false);
        this.x_max = x_max;
        this.y_max = y_max;
        this.name = name;
        this.generateLimits();

    }

    public ScurWorld(float x_max, float y_max, Sprite sprite, String name){
        this(x_max, y_max, name);
        this.sprite = sprite;
        //this.sprite.setPosition(0f, GUI.getInstance().getSize().y - sprite.getTexture().getSize().y / Utils.scale);
    }


    public Entity createRectangularEntity(float x, float y, float width, float height, boolean dynamic){

        x += width/2f;
        y += height/2f;

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = dynamic ? BodyType.DYNAMIC : BodyType.STATIC;
        bodyDef.position.set(new Vec2(x, y));
        bodyDef.fixedRotation = true;
        Body b = this.createBody(bodyDef);

        PolygonShape polygonShape = new PolygonShape();
        polygonShape.setAsBox(width / 2f, height / 2f);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = polygonShape;
        fixtureDef.density = 0f;
        fixtureDef.friction = 0f;


        b.createFixture(fixtureDef);
        //b.resetMassData();

        return new Entity(b, width, height);
    }


    public Entity createEntityFromSprite(float x, float y, boolean dynamic, String imgPath){
        Sprite sprite = new SpriteFactoryForEntity().loadSprite(imgPath);

        float width = sprite.getTexture().getSize().x / SpriteFactory.scale;
        float height = sprite.getTexture().getSize().y / SpriteFactory.scale;

        Entity e = this.createRectangularEntity(x, y, width, height, dynamic);
        e.setSprite(sprite);

        return e;

    }

    private void generateLimits(){
        boolean invisible = true;
        this.createRectangularEntity(-0.1f, 0, 0.1f, this.y_max, false).setInvisible(invisible);
        this.createRectangularEntity(this.x_max, 0, 0.1f, this.y_max, false).setInvisible(invisible);
        this.createRectangularEntity(0, this.y_max, this.x_max, 0.1f, false).setInvisible(invisible);
        this.createRectangularEntity(0, -0.1f, this.x_max, 0.1f, false).setInvisible(invisible);
    }

    /*
    #################### GETTERS #####################
     */

    public float getX_max() {
        return x_max;
    }

    public float getY_max() {
        return y_max;
    }

    public Sprite getSprite(){
        return this.sprite;
    }

    /*
    ################## DISPLAY ###################
     */

    @Override
    public void draw(RenderTarget renderTarget, RenderStates renderStates) {
        if(this.sprite == null){
            RectangleShape rectangleShape = new RectangleShape();
            rectangleShape.setSize(new Vector2f(1f, 1f));
            rectangleShape.setFillColor(Color.WHITE);

            for(int i = 0; i < x_max; i ++){
                int start = 0;
                if(i % 2 == 1) {
                    start = 1;
                }
                for(int j = start; j < y_max; j += 2){
                    int y = -j;
                    y += renderTarget.getSize().y;
                    y -= 1;
                    rectangleShape.setPosition(new Vector2f(i, y));
                    renderTarget.draw(rectangleShape);
                }
            }
        }
        else{
            float x = 0;
            float y = GUI.getInstance().getSize().y - y_max;
            this.sprite.setPosition(x, y);

            renderTarget.draw(this.sprite);
        }
    }
}
