package Model.Entities;

import org.jbox2d.dynamics.Body;
import org.jsfml.graphics.*;
import org.jsfml.graphics.Color;
import org.jsfml.system.Vector2f;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;


public class Entity implements Drawable {

    public static HashMap<Body, Entity> entityMap = new HashMap<>();

    //for physics
    private Body body;

    //for displaying
    private boolean invisible;
    private RectangleShape rectangleShape;
    private Sprite sprite;



    public Entity(Body b, float width, float heigth){
        this.body = b;

        rectangleShape = new RectangleShape(new Vector2f(width, heigth));
        Color color = new Color((int)(Math.random()*255), (int)(Math.random()*255), (int)(Math.random()*255));
        rectangleShape.setFillColor(color);

        entityMap.put(b, this);
        this.invisible = false;
    }


    @Override
    public void draw(RenderTarget renderTarget, RenderStates renderStates) {
        if(this.invisible)
            return;
        float x = body.getPosition().x;
        float y = body.getPosition().y;
        x -= rectangleShape.getSize().x/2;
        y *= -1;
        y += renderTarget.getSize().y;
        y -= rectangleShape.getSize().y/2;

        rectangleShape.setPosition(new Vector2f(x, y));

        if(this.sprite == null){
            renderTarget.draw(rectangleShape);
        }
        else{
            sprite.setPosition(this.rectangleShape.getPosition());
            renderTarget.draw(sprite);
        }
    }


    /*
    ############## GETTERS ################
     */

    public RectangleShape getRectangleShape() {
        return rectangleShape;
    }

    public Body getBody(){
        return this.body;
    }

    public String toString(){
        return "Entity[x="+body.getPosition().x+", y="+body.getPosition().y+", width="+rectangleShape.getSize().x+", height="+rectangleShape.getSize().y+"]";
    }

    /*
    ############## SETTERS ####################
     */

    public void setSprite(Sprite s){
        this.sprite = s;
    }

    public void setInvisible(boolean invisible) {
        this.invisible = invisible;
    }
}
