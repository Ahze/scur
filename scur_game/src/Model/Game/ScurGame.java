package Model.Game;

import GameEngine.AbstractGame;
import Model.Entities.Entity;
import Model.World.ScurWorld;
import Model.World.WorldFactory.WorldFactoryFromFolder;
import Model.World.WorldFactory.WorldFactory;
import Model.World.WorldFactory.WorldGeneratorFromInts;
import Model.World.WorldListener;
import org.jbox2d.common.Vec2;
import org.jsfml.window.Keyboard;


public class ScurGame implements AbstractGame {

    private static ScurGame instance;

    private ScurWorld world;
    private WorldFactory worldFactory;
    private Entity player;
    private boolean over;


    private ScurGame(){
        //initialisation
        this.worldFactory = new WorldFactoryFromFolder("minishcap");
        this.world = this.worldFactory.createWorld();
        this.player = this.world.createEntityFromSprite(1, 1, true, "player.png");
        this.world.setContactListener(new WorldListener(player.getBody().getFixtureList()));
        this.over = false;
    }

    private float desired_speed = 5f;

    @Override
    public String update() {
        this.world.step(1.0f/60.0f /*  division de floats */, 6, 2  );

        float actual_speed_x = player.getBody().getLinearVelocity().x;
        float actual_speed_y = player.getBody().getLinearVelocity().y;
        float mass = player.getBody().getMass();

        if(Keyboard.isKeyPressed(Keyboard.Key.SPACE)){
            player.getBody().applyLinearImpulse(new Vec2(0f, 3f), player.getBody().getLocalCenter());
        }

        if(Keyboard.isKeyPressed(Keyboard.Key.Q)){
            player.getBody().applyForce(new Vec2((-actual_speed_x - desired_speed)*mass, 0), player.getBody().getLocalCenter());
        }
        else if(Keyboard.isKeyPressed(Keyboard.Key.D)){
            player.getBody().applyForce(new Vec2((desired_speed - actual_speed_x)*mass, 0f), player.getBody().getLocalCenter());
        }
        else{
            //player.getBody().getLinearVelocity().x = 0;
        }

        if(Keyboard.isKeyPressed(Keyboard.Key.Z)){
            player.getBody().applyForce(new Vec2(0f, (desired_speed - actual_speed_y)*mass), player.getBody().getLocalCenter());
        }
        else if(Keyboard.isKeyPressed(Keyboard.Key.S)){
            player.getBody().applyForce(new Vec2(0f , (-actual_speed_y - desired_speed)*mass), player.getBody().getLocalCenter());
        }
        else{
            //player.getBody().getLinearVelocity().y = 0;
        }



        if(Keyboard.isKeyPressed(Keyboard.Key.ESCAPE)){
            this.stop();
        }


        return null;
    }


    public void stop(){
        this.over = true;
    }

    /*
    ############# GETTERS ################
     */

    public ScurWorld getWorld() {
        return world;
    }

    public Entity getPlayer(){
        return this.player;
    }

    @Override
    public boolean isOver() {
        return this.over;
    }

    /*
    ############## Static methods ##############
     */

    public static ScurGame getInstance() {
        return instance;
    }

    public static void createInstance(){
        instance = new ScurGame();
    }

    public static void restart(){
        instance = new ScurGame();
    }
}
