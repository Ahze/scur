package WorldEditor;

import javax.swing.*;
import java.awt.*;

public class EditorMain{

    public static void main(String[] args) {
        JFrame window = new JFrame("World Editor");
        EditorFrame editorFrame = new EditorFrame(new Model());
        window.setContentPane(editorFrame);

        editorFrame.setFocusable(true);

        editorFrame.setPreferredSize(new Dimension(800, 600));

        window.setDefaultCloseOperation(3);
        window.pack();
        window.setVisible(true);

        window.requestFocus();
        editorFrame.requestFocus();
        editorFrame.requestFocusInWindow();
    }
}
