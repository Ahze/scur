package WorldEditor;

import Display.SpriteFacory.SpriteFactory;
import org.jsfml.window.Keyboard;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class InputControler  extends JPanel implements MouseMotionListener, MouseWheelListener, MouseListener, KeyListener {

    /*
    ########### ATTRIBUTES ##########
     */
    private Model model;
    private EditorFrame vue;

    private Mode mode;
    private Point p1, p2;
    private Rectangle rectangle;

    private JComboBox modeChooser;

    /*
    ############### CONSTRUCTOR ##############
     */

    public InputControler(Model model, EditorFrame vue) {
        super();
        this.model = model;
        this.vue = vue;
        this.mode = Mode.CreateHitBox;



        modeChooser = new JComboBox(new String[]{"Creer Hitbox Manuelement (C)", "Creer HitBox avec la grille (G)", "Modifier Hitbox (M)"});
        JCheckBox grid = new JCheckBox("Afficher la grille");


        modeChooser.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                switch ( ((JComboBox)actionEvent.getSource()).getSelectedIndex() ){
                    case 0:
                        mode = Mode.CreateHitBox;
                        break;
                    case 1:
                        mode = Mode.GridHitBox;
                        break;
                    case 2:
                        mode = Mode.MoveHitBox;
                        break;
                }
                vue.requestFocus();
                vue.requestFocusInWindow();
            }
        });

        grid.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                vue.drawGrid = !vue.drawGrid;
                vue.repaint();
            }
        });

        add(modeChooser);
        add(grid);

        setBackground(new Color(68, 69, 75));
    }

    /*
    ############ METHODS FROM MOUSELISTENER ################
     */
    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        Point click = getRealPoint(mouseEvent.getX(), mouseEvent.getY());
        if(pointOutOfBounds(click))
            return;

        switch (mode){
            case CreateHitBox:

                break;
            case MoveHitBox:
                for(Rectangle r : model.getRectangleList()){
                    if(r.contains(click)){
                        this.rectangle = r;
                        vue.repaint();
                        return;
                    }
                }
                this.rectangle = null;
                vue.repaint();
                break;
            case GridHitBox:
                int x = click.x - (int)((float)click.x % SpriteFactory.scale);
                int y = click.y - (int)((float)click.y % SpriteFactory.scale);
                model.addRectangle(new Rectangle(x, y, (int)SpriteFactory.scale, (int)SpriteFactory.scale));

        }
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {
        Point point = getRealPoint(mouseEvent.getX(), mouseEvent.getY());;
        if(pointOutOfBounds(point))
            return;

        switch(mode){
            case CreateHitBox:
                if(mouseEvent.getX() < 0 || mouseEvent.getY() < 0) {
                    return;
                }
                p1 = point;
                p2 = p1;
                break;
            case MoveHitBox:
                if(rectangle != null){
                    p1 = getRealPoint(mouseEvent.getX(), mouseEvent.getY());
                    p1.x -= rectangle.x;
                    p1.y -= rectangle.y;
                    System.out.println("x="+p1.x+" y="+p1.y);
                }
                break;
        }
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {
        if(pointOutOfBounds(getRealPoint(mouseEvent.getX(), mouseEvent.getY())))
            return;

        if(mode == Mode.CreateHitBox){
            this.model.addRectangle(getRectangle());
            p1 = null;
        }
        vue.repaint();
    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }

    /*
    ############ METHODS FROM MOUSEMOTIONLISTENER ################
     */

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {
        Point point = getRealPoint(mouseEvent.getX(), mouseEvent.getY());
        if(pointOutOfBounds(point))
            return;
        switch(mode){
            case CreateHitBox:
                if(mouseEvent.getX() < 0 || mouseEvent.getY() < 0) {
                    return;
                }
                p2 = point;
                vue.repaint();
                break;
            case GridHitBox:
                this.mouseClicked(mouseEvent);
                break;
            case MoveHitBox:
                if(this.rectangle != null){
                    p2 = getRealPoint(mouseEvent.getX(), mouseEvent.getY());
                    p2.x -= p1.x;
                    p2.y -= p1.y;
                    rectangle.x = p2.x;
                    rectangle.y = p2.y;
                    vue.repaint();
                }
                break;
        }
    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {

    }


    /*
    ############ METHODS FROM MOUSEWHEELLISTENER ################
     */

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        if(Keyboard.isKeyPressed(Keyboard.Key.LCONTROL)){
            if(e.getWheelRotation()<0) {
                vue.zoom();
            }
            else {
                vue.unzoom();
            }
        }
        else {
            if(Keyboard.isKeyPressed(Keyboard.Key.LSHIFT)){
                vue.dx -= e.getWheelRotation()*(model.getImage().getWidth(null)/15);
            }
            else {
                vue.dy -= e.getWheelRotation()*(model.getImage().getHeight(null)/15);
            }
        }
        vue.repaint();
    }

     /*
    ############ METHODS FROM KEYLISTENER ################
     */



    @Override
    public void keyTyped(KeyEvent keyEvent) {

    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        switch (keyEvent.getKeyCode()){
            case KeyEvent.VK_C:
                mode = Mode.CreateHitBox;
                modeChooser.setSelectedIndex(0);
                break;
            case KeyEvent.VK_G:
                mode = Mode.GridHitBox;
                modeChooser.setSelectedIndex(1);
                break;
            case KeyEvent.VK_M:
                mode = Mode.MoveHitBox;
                modeChooser.setSelectedIndex(2);
                break;
        }
        switch (mode){
            case MoveHitBox:
                if(keyEvent.getKeyCode() == KeyEvent.VK_DELETE){
                    model.getRectangleList().remove(this.rectangle);
                    this.rectangle = null;
                    vue.repaint();
                }
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {

    }

    /*
    ############### OTHER METHODS  #############
     */

    public Rectangle getRectangle(){
        switch (mode){
            case CreateHitBox:
                if(p1 == null) {
                    return null;
                }

                int x, y, width, height;
                x = p1.x < p2.x ? p1.x : p2.x;
                y = p1.y < p2.y ? p1.y : p2.y;

                width = Math.abs(p1.x - p2.x);
                height = Math.abs(p1.y - p2.y);

                return new Rectangle(x, y, width, height);
            default:
                return this.rectangle;
        }
    }

    private Point getRealPoint(int x, int y){
        int nx = (int)( (float)x / vue.zoomFactor - vue.dx );
        int ny = (int)( (float)y / vue.zoomFactor - vue.dy);
        return new Point(nx, ny);
    }

    private boolean pointOutOfBounds(Point p){
        if(model.getImage() == null)
            return true;
        return p.x < 0 || p.y < 0 || p.x >= model.getImage().getWidth(null) || p.y >= model.getImage().getHeight(null);
    }


    enum Mode{
        CreateHitBox,
        GridHitBox,
        MoveHitBox
    }
}
