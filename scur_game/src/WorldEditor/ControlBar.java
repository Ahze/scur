package WorldEditor;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class ControlBar extends JPanel {

    private Model model;
    private EditorFrame vue;

    private JTextField zoomDisplay;
    JTextField name;

    public ControlBar(Model model, EditorFrame vue) {
        super();
        this.model = model;
        this.vue = vue;

        JButton open = new JButton("Open picture");
        open.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JFileChooser chooser = new JFileChooser();
                chooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
                chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                int returnVal = chooser.showOpenDialog(new JPanel());
                if(returnVal == JFileChooser.APPROVE_OPTION) {
                    model.setImage(chooser.getSelectedFile().toString()+"/sprite.png");
                    File f = new File(chooser.getSelectedFile().toString()+"/details");
                    if(f.exists()){
                        model.loadDetails(f);
                    }
                    vue.repaint();
                }
                vue.requestFocus();
                vue.requestFocusInWindow();
            }
        });
        JButton save = new JButton("Save Json");
        save.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new RectanglesToJson(model).save(name.getText());
            }
        });
        name = new JTextField(20);

        zoomDisplay = new JTextField("1", 5);
        zoomDisplay.setEnabled(false);


        add(open);
        add(save);
        add(name);
        add(zoomDisplay);

        setBackground(new Color(53, 54, 60));
    }

    public void setZoomDisplay(float zoom){
        String s = String.format("%.2f", zoom);
        this.zoomDisplay.setText(s);
    }
}
