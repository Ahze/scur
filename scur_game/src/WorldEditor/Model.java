package WorldEditor;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.Observable;

public class Model extends Observable {

    private Image image;
    private String imgPath, worldName;
    private ArrayList<Rectangle> rectangleList;

    public Model(){
        super();
        this.rectangleList = new ArrayList<>();
    }

    public Model(String imgPath){
        this();
        this.setImage(imgPath);
    }

    public void setImage(String imgPath){
        try{
            System.out.println(imgPath);
            image = ImageIO.read(new File(imgPath));
            this.imgPath = imgPath;
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void addRectangle(Rectangle rectangle){
        for(Rectangle r : this.rectangleList){
            if(r.intersects(rectangle)){
                return;
            }
        }
        this.rectangleList.add(rectangle);
        setChanged();
        notifyObservers();
    }

    public void loadDetails(File f){
        RectanglesToJson.load(f, this);
    }

    public Image getImage() {
        return image;
    }


    public ArrayList<Rectangle> getRectangleList() {
        return rectangleList;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setName(String name){
        this.worldName = name;
    }

    public void resetList(){
        this.rectangleList = new ArrayList<>();
    }
}
