package WorldEditor;

import Display.SpriteFacory.SpriteFactory;

import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;

public class EditorFrame extends JPanel implements Observer {

    private Model model;
    private InputControler inputControler;
    private ControlBar controlBar;
    private DrawPanel drawPanel;

    public int dx = 0, dy = 0;
    public float zoomFactor = 1;
    public boolean drawGrid = false;

    //TODO redonner le focus apres la selection d'un fichier

    public EditorFrame(Model model) {
        super();
        drawPanel = new DrawPanel();
        this.model = model;
        this.model.addObserver(this);
        inputControler = new InputControler(model, this);
        controlBar = new ControlBar(model, this);
        drawPanel.addMouseListener(inputControler);
        drawPanel.addMouseMotionListener(inputControler);
        drawPanel.addMouseWheelListener(inputControler);

        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

        this.add(controlBar);
        this.add(inputControler);
        this.add(drawPanel);
        this.addKeyListener(inputControler);

        drawPanel.setBackground(new Color(108, 109, 116));
    }

    @Override
    public void update(Observable observable, Object o) {
        this.repaint();
    }


    public void zoom(){
        this.zoomFactor += 0.1f;
    }

    public void unzoom(){
        this.zoomFactor -= 0.1f;
    }

    @Override
    public void setPreferredSize(Dimension dim){
        super.setPreferredSize(dim);
        controlBar.setPreferredSize(new Dimension(dim.width, 35));
        inputControler.setPreferredSize(new Dimension(dim.width, 35));
        drawPanel.setPreferredSize(new Dimension(dim.width, dim.height-70));
    }

    class DrawPanel extends JPanel{

        DrawPanel(){
            super();
        }

        private final int initialPointSize = 12;
        Color gridColor = new Color(0.5f, 0.1f, 0.1f, 0.5f);
        Color pointsColor = new Color(0.83f, 0.5f, 0.12f, 0.9f);

        public void paintComponent(Graphics g){
            super.paintComponent(g);

            Graphics2D g2 = (Graphics2D)g.create();
            g2.scale(zoomFactor, zoomFactor);
            g2.translate(dx, dy);

            g2.drawImage(model.getImage(), 0, 0, null);

            g2.setColor(new Color(0.1f, 0.1f, 0.5f, 0.5f));
            for(Rectangle r : model.getRectangleList()){
                g2.fillRect(r.x, r.y, r.width, r.height);
            }
            if(inputControler.getRectangle() != null) {
                Rectangle r = inputControler.getRectangle(); //selected rectangle
                int pointSize = (int)((float)initialPointSize/zoomFactor);
                g2.fillRect(r.x, r.y, r.width, r.height);
                g2.setColor(pointsColor);
                g2.fillOval(r.x - pointSize/2, r.y - pointSize/2, pointSize, pointSize);
                g2.fillOval(r.x + r.width - pointSize/2, r.y - pointSize/2, pointSize, pointSize);
                g2.fillOval(r.x - pointSize/2, r.y + r.height - pointSize/2, pointSize, pointSize);
                g2.fillOval(r.x + r.width - pointSize/2, r.y + r.height - pointSize/2, pointSize, pointSize);
                g2.setColor(Color.BLACK);
                g2.drawOval(r.x - pointSize/2, r.y - pointSize/2, pointSize, pointSize);
                g2.drawOval(r.x + r.width - pointSize/2, r.y - pointSize/2, pointSize, pointSize);
                g2.drawOval(r.x - pointSize/2, r.y + r.height - pointSize/2, pointSize, pointSize);
                g2.drawOval(r.x + r.width - pointSize/2, r.y + r.height - pointSize/2, pointSize, pointSize);
            }

            if(drawGrid){
                g2.setColor(gridColor);
                int width = model.getImage().getWidth(null);
                int height = model.getImage().getHeight(null);
                for(int i = 0; i < width; i+= SpriteFactory.scale){
                    g2.drawLine(i, 0, i, height);
                }
                for(int i = 0; i < height; i+= SpriteFactory.scale){
                    g2.drawLine(0, i, width, i);
                }
            }

            g2.dispose();

            controlBar.setZoomDisplay(EditorFrame.this.zoomFactor);

        }

    }
}
