package WorldEditor;

import Display.SpriteFacory.SpriteFactory;
import org.json.JSONArray;
import org.json.JSONObject;

import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

public class RectanglesToJson {

    private Model container;

    public RectanglesToJson(Model container) {
        this.container = container;
    }

    public void save(String name){
        String folder = "";
        String[] tmp = container.getImgPath().split("/");
        for(int i = 0; i < tmp.length-1; i++){
            folder += tmp[i] + "/";
        }
        System.out.println(folder);
        try{
            JSONObject o = new JSONObject();
            o.put("name", name);

            JSONObject[] rects = new JSONObject[container.getRectangleList().size()];
            int i = 0;
            for(Rectangle r : container.getRectangleList()){
                JSONObject rect = new JSONObject();
                rect.put("x", r.x / SpriteFactory.scale);
                float y = container.getImage().getHeight(null) - r.y - r.height;
                y /= SpriteFactory.scale;
                rect.put("y", y);
                rect.put("width", r.width / SpriteFactory.scale);
                rect.put("height", r.height / SpriteFactory.scale);
                rects[i++] = rect ;
            }
            JSONArray array = new JSONArray(rects);

            o.put("walls", array);

            System.out.println(o.toString());
            try(FileWriter fw = new FileWriter(folder+"details")){
                fw.write(o.toString());
            }


        }catch (Exception e){

        }

    }

    public static void load(File f, Model model){
        try{
            BufferedReader br = new BufferedReader(new FileReader(f));
            String json = "", line;
            while( (line = br.readLine()) != null){
                json += line;
            }
            JSONObject o = new JSONObject(json);
            model.setName(o.getString("name"));
            model.resetList();
            JSONArray walls = o.getJSONArray("walls");
            for(int i = 0; i < walls.length(); i++){
                JSONObject wall = (JSONObject)walls.get(i);
                int width = (int)(wall.getDouble("width")*SpriteFactory.scale);
                int height = (int)(wall.getDouble("height")*SpriteFactory.scale);
                int x = (int)(wall.getDouble("x")*SpriteFactory.scale);
                int y = (int)(wall.getDouble("y")*SpriteFactory.scale);
                y = model.getImage().getHeight(null) - y - height;
                model.addRectangle(new Rectangle(x, y, width, height));
            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
