package Display.SpriteFacory;

import org.jsfml.graphics.Sprite;
import org.jsfml.graphics.Texture;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SpriteFactoryForWorld implements SpriteFactory {

    @Override
    public Sprite loadSprite(String worldName) {
        Sprite sprite = null;
        try{
            Path path = Paths.get(new File("datas/worlds/"+worldName+"/sprite.png").toURI());
            Texture texture = new Texture();
            texture.loadFromFile(path);
            texture.setRepeated(true);

            sprite = new Sprite(texture);
            sprite.setScale(1f/scale, 1f/scale);
        }catch (Exception e){
            System.out.println("Can not load sprite : "+worldName);
            e.printStackTrace();
        }
        return sprite;
    }
}
