package Display.SpriteFacory;

import org.jsfml.graphics.Sprite;

public interface SpriteFactory {

    float scale = 20; //50 pixels per meter

    Sprite loadSprite(String imgName);
}
