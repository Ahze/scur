package Display.SpriteFacory;

import org.jsfml.graphics.Sprite;
import org.jsfml.graphics.Texture;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SpriteFactoryForEntity implements SpriteFactory {

    @Override
    public Sprite loadSprite(String imgName) {
        Sprite sprite = null;
        try{
            Path path = Paths.get(new File("datas/sprites/"+imgName).toURI());
            Texture texture = new Texture();
            texture.loadFromFile(path);
            texture.setRepeated(true);

            sprite = new Sprite(texture);
            sprite.setScale(1f/scale, 1f/scale);
        }catch (Exception e){
            System.out.println("Can not load sprite : "+imgName);
            e.printStackTrace();
        }
        return sprite;
    }
}
