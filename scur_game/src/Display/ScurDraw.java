package Display;

import Display.AbstractDrawing;
import Model.Entities.Entity;
import Model.Game.ScurGame;
import org.jbox2d.dynamics.Body;
import org.jsfml.graphics.*;


public class ScurDraw implements AbstractDrawing {

    @Override
    public void draw(RenderWindow window) {
        window.clear(Color.BLACK);

        window.draw(ScurGame.getInstance().getWorld());

        Body b = ScurGame.getInstance().getWorld().getBodyList();
        while(b != null){
            window.draw(Entity.entityMap.get(b));
            b = b.getNext();
        }

        window.display();
    }



}
