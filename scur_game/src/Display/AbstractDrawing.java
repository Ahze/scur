package Display;

import org.jsfml.graphics.RenderWindow;

/**
 * une interface representant la maniere de draw sur un JPanel
 * 
 * @author vthomas
 */
public interface AbstractDrawing {

	/**
	 * methode draw a completer. Elle construit une image correspondant au
	 * jeu. Jeu est un attribut de l'afficheur
	 * 
	 * @param image
	 *            image sur laquelle draw
	 */
	public void draw(RenderWindow window);

}
