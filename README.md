#Projet par AhzeLeSteak


Arborescence des packages et classes :

+   Display
    +   SpriteFactory
        +   SpriteFactory
        +   SpriteFactoryForEntity
        +   SpriteFactoryForWorld
    +   AbstractDrawing
    +   ScurDraw
+   GameEngine
    +   AbstractGame
    +   Engine
    +   GUI
+   Main
    +   Main
+   Model
    +   Entities
        +   Entity
    +   Game
        +   ScurGame
    +   World
        +   WorldFactory
            +   WorldFactoryFromFolder
            +   WorldFactoryFromInts
        +   ScurWorld
+   WorldEditor
    +   ControlBar
    +   EditorFrame
    +   EditorMain
    +   InputControler
    +   Model
    +   RectanglesToJson
    
Diagramme de classe du jeu :
![diagram class](./docs/classes_diagram.png)
    
    